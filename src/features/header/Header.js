import React from 'react';
import { Link } from "react-router-dom";

export function Header() {
  return (
    <div>
      <Link to="/">Inicio</Link>
      <Link to="/products">Productos</Link>
      <Link to="/promotions">Promociones</Link>
      <Link to="/cart">Carrito</Link>
    </div>
  )
}
