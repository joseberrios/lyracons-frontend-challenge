import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

// import { Counter } from './features/counter/Counter';
import { Header } from './features/header/Header';

import './App.css';

function App() {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
          <Route exact path="/">
            <div>
              <h1>Home Page</h1>
            </div>
          </Route>
          <Route path="/products">
            <div>
              <h1>Productos Page</h1>
            </div>
          </Route>
          <Route path="/promotions">
            <div>
              <h1>Promociones Page</h1>
            </div>
          </Route>
          <Route path="/cart">
            <div>
              <h1>Carrito Page</h1>
            </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
